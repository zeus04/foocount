<?php
/*
Inside Apache <VirtualHost>:

RewriteEngine On
RewriteRule ^/file-directory-to-monitor /foocount.php?file=%{REQUEST_URI}

 */

$path = $_GET['file'];
if (empty($path)) notFound();

$root = "/var/www/html";
$dir = realpath(dirname($root . '/' . $path ));

if ($dir === FALSE) notFound();

$file = basename($path);
$ext = pathinfo($file, PATHINFO_EXTENSION);

# Whitelist of allowed file extensions
#if (!in_array(strtolower($ext), array('pdf', 'doc', 'jpg', 'png', 'gif', 'xls', 'docx', 'odt', 'ppt'))) notFound();

if ($fh = opendir($dir)) {
	while (false !== ($tfile = readdir($fh))) {
   		if ($tfile == "." || $tfile == "..") continue;
   		if (strtolower($tfile) != strtolower($file)) continue;

		if(!is_bot()) {
			$msg = sprintf('%s: %s [%s] (%s) laddade ner %s från modell-bilder.se', date("Y-m-d, H:i"), $_SERVER['REMOTE_ADDR'], gethostbyaddr($_SERVER['REMOTE_ADDR']), $_SERVER['HTTP_USER_AGENT'], $tfile);
			mail("foocount-debian@fashionmedia.se", "Ny nedladdning: $tfile från modell-bilder.se", $msg);
			file_put_contents('/var/www/html/dl.log', $msg.PHP_EOL , FILE_APPEND);
		}	
		$content_type = mime_content_type($dir.'/'.$tfile);
		header("Cache-Control: public");
		header("Content-Length: ".filesize( $dir.'/'.$tfile ) );
		header("Content-Type: $content_type");
		ob_clean();
		ob_end_flush();
		readfile($dir.'/'.$tfile);
	}
}

function is_bot() {
    $botlist = array("Teoma", "alexa", "froogle", "Gigabot", "inktomi",
    "looksmart", "URL_Spider_SQL", "Firefly", "NationalDirectory",
    "Ask Jeeves", "TECNOSEEK", "InfoSeek", "WebFindBot", "girafabot",
    "crawler", "www.galaxy.com", "Googlebot", "Scooter", "Slurp",
    "msnbot", "appie", "FAST", "WebBug", "Spade", "ZyBorg", "rabaz",
    "Baiduspider", "Feedfetcher-Google", "TechnoratiSnoop", "Rankivabot",
    "Mediapartners-Google", "Sogou web spider", "WebAlta Crawler","TweetmemeBot",
    "Butterfly","Twitturls","Me.dium","Twiceler","facebookexternalhit/1.1","bitlybot/3.0 (+http://bit.ly/)","bitlybot");

    foreach($botlist as $bot) {
        if(strpos($_SERVER['HTTP_USER_AGENT'],$bot)!==false)
        	return true;    // Is a bot
    }

    return false;
}

function notFound() {
	header("HTTP/1.1 404 Not Found"); 
	exit();
}

